# frozen_string_literal: true

require 'cgi'

module Util
  def h(string)
    CGI.escape_html(string)
  end
end
