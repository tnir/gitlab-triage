require 'spec_helper'

require 'gitlab/triage/api_query_builders/date_query_param_builder'

describe Gitlab::Triage::APIQueryBuilders::DateQueryParamBuilder do
  let(:conditions) do
    {
      attribute: 'created_at',
      condition: 'older_than',
      interval_type: 'days',
      interval: 10
    }
  end

  describe '.applicable?' do
    context 'when conditions are valid' do
      it 'returns true' do
        expect(described_class.applicable?(conditions)).to eq(true)
      end
    end

    context 'when attribute is invalid' do
      before do
        conditions[:attribute] = 'merged_at'
      end

      it 'returns false' do
        expect(described_class.applicable?(conditions)).to eq(false)
      end
    end
  end

  describe '#validate_condition' do
    context 'when condition is invalid' do
      before do
        conditions[:condition] = 'unknown'
      end

      it 'raises InvalidParameter' do
        expect { described_class.new(conditions) }.to raise_error(Gitlab::Triage::ParamsValidator::InvalidParameter)
      end
    end

    context 'when interval_type is invalid' do
      before do
        conditions[:interval_type] = 'unknown'
      end

      it 'raises InvalidParameter' do
        expect { described_class.new(conditions) }.to raise_error(Gitlab::Triage::ParamsValidator::InvalidParameter)
      end
    end

    context 'when interval is invalid' do
      before do
        conditions[:interval] = '10'
      end

      it 'raises InvalidParameter' do
        expect { described_class.new(conditions) }.to raise_error(Gitlab::Triage::ParamsValidator::InvalidParameter)
      end
    end
  end

  describe '#param_name' do
    context 'when attribute is created_at' do
      context 'when condition is older_than' do
        before do
          conditions[:condition] = 'older_than'
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_name).to eq('created_before')
        end
      end

      context 'when condition is newer_than' do
        before do
          conditions[:condition] = 'newer_than'
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_name).to eq('created_after')
        end
      end
    end

    context 'when attribute is updated_at' do
      before do
        conditions[:attribute] = 'updated_at'
      end

      context 'when condition is older_than' do
        before do
          conditions[:condition] = 'older_than'
        end

        it 'returns updated_before' do
          expect(described_class.new(conditions).param_name).to eq('updated_before')
        end
      end

      context 'when condition is newer_than' do
        before do
          conditions[:condition] = 'newer_than'
        end

        it 'returns updated_before' do
          expect(described_class.new(conditions).param_name).to eq('updated_after')
        end
      end
    end
  end

  describe '#param_content' do
    around do |example|
      travel_to(Time.utc(2020, 5, 15, 9, 30)) do
        example.run
      end
    end

    context 'when interval_type is minutes' do
      before do
        conditions[:interval_type] = 'minutes'
      end

      context 'when interval is 2' do
        before do
          conditions[:interval] = 2
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_content).to eq(Time.utc(2020, 5, 15, 9, 28))
        end
      end
    end

    context 'when interval_type is hours' do
      before do
        conditions[:interval_type] = 'hours'
      end

      context 'when interval is 2' do
        before do
          conditions[:interval] = 2
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_content).to eq(Time.utc(2020, 5, 15, 7, 30))
        end
      end
    end

    context 'when interval_type is days' do
      before do
        conditions[:interval_type] = 'days'
      end

      context 'when interval is 2' do
        before do
          conditions[:interval] = 2
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_content).to eq(Time.utc(2020, 5, 13))
        end
      end
    end

    context 'when interval_type is months' do
      before do
        conditions[:interval_type] = 'months'
      end

      context 'when interval is 2' do
        before do
          conditions[:interval] = 2
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_content).to eq(Time.utc(2020, 3, 15))
        end
      end
    end

    context 'when interval_type is years' do
      before do
        conditions[:interval_type] = 'years'
      end

      context 'when interval is 2' do
        before do
          conditions[:interval] = 2
        end

        it 'returns created_before' do
          expect(described_class.new(conditions).param_content).to eq(Time.utc(2018, 5, 15))
        end
      end
    end
  end
end
