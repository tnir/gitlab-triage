# frozen_string_literal: true

require 'spec_helper'

describe 'select issues by health status' do
  include_context 'with integration context'

  let(:issue_without_health_status) do
    issue.merge(description: 'issue without health status')
  end
  let(:issue_with_on_track_health_status) do
    issue.merge(description: 'issue with on track health status', iid: issue[:iid] + 1)
  end
  let(:issue_with_needs_attention_health_status) do
    issue.merge(description: 'issue with needs attention health status', iid: issue[:iid] + 2)
  end
  let(:issue_with_at_risk_health_status) do
    issue.merge(description: 'issue with at risk health status', iid: issue[:iid] + 3)
  end

  before do
    stub_issues_api_with_query(health_status, resource)
  end

  shared_examples 'matched rule' do
    it 'posts the correct comment on the issue' do
      stub_post = stub_api_comment(resource[:iid], comment)

      perform(rule(health_status, comment))

      assert_requested(stub_post)
    end
  end

  context 'without health status' do
    let(:health_status) { 'none' }
    let(:resource) { issue_without_health_status }
    let(:comment) { 'Comment because it does not have a health status.' }

    it_behaves_like 'matched rule'
  end

  context 'with any health status' do
    let(:health_status) { 'any' }
    let(:resource) { issue_with_on_track_health_status }
    let(:comment) { 'Comment because it does have a health status.' }

    it_behaves_like 'matched rule'
  end

  context 'when health status is on_track' do
    let(:health_status) { 'on_track' }
    let(:resource) { issue_with_on_track_health_status }
    let(:comment) { 'Comment because health_status is on_track.' }

    it_behaves_like 'matched rule'
  end

  context 'when health status is needs_attention' do
    let(:health_status) { 'needs_attention' }
    let(:resource) { issue_with_needs_attention_health_status }
    let(:comment) { 'Comment because health_status is needs_attention.' }

    it_behaves_like 'matched rule'
  end

  context 'when health status is at_risk' do
    let(:health_status) { 'at_risk' }
    let(:resource) { issue_with_at_risk_health_status }
    let(:comment) { 'Comment because health_status is at_risk.' }

    it_behaves_like 'matched rule'
  end

  def stub_api_comment(iid, body)
    stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{iid}/notes",
      body: { body: body },
      headers: { 'PRIVATE-TOKEN' => token })
  end

  def stub_issues_api_with_query(health_status, issue)
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100, health_status: health_status },
      headers: { 'PRIVATE-TOKEN' => token }) do
        [issue]
      end
  end

  def rule(health_status, comment)
    <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Test rule
              conditions:
                health_status: #{health_status}
              actions:
                comment: |
                  #{comment}
    YAML
  end
end
